FROM python:3.8-slim

MAINTAINER Your Name (your.email@company.org)

ENV PYTHONPATH /app
ENV PYTHONUNBUFFERED=1

COPY module /app/module
COPY main.py /app/main.py
COPY requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

ENTRYPOINT ["python", "/app/main.py"]