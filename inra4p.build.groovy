def build() {

    stage('Parsing version') {
        VERSION = sh(
			script: "git describe",
			returnStdout: true
			).trim()
        echo("Version=${VERSION}")
    }

    stage('Docker build') {
        sh('echo "Docker build"')
        sh("docker build -t my-module:${VERSION} .")
    }

    stage('Docker test') {
        sh('echo "Docker test"')
        sh("docker run --rm -v ${WORKSPACE}/test:/data -w /data my-module:${VERSION} file 2 output")
    }
}

return this
