import unittest
from datetime import datetime

DATETIME_FORMAT = "%d/%m/%Y %H:%M:%S"

from module import uav_rgb_objectdetection_all


class TestCore(unittest.TestCase):
    """
    Test case for core methods

    TODO: Add other test cases. For help refer to this tutorial: https://docs.python.org/3/library/unittest.html
    """

    def test_run(self):
        core.run(file='file.csv',
                 scalar=2,
                 option=0,
                 datetime=datetime.strptime('01/01/2021 10:10:10', DATETIME_FORMAT),
                 output_folder='output')


if __name__ == '__main__':
    unittest.main()
