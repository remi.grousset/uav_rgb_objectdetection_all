"""
This module takes as argument the path to a folder containing images with objects to detect, and the paths to the config
file and the weights of the MaskRCNN model used. It then detects the objects on the images and stores it into a json
file in coco format
"""


import json
from datetime import datetime
from pathlib import Path
import itertools
import logging

import cv2
import matplotlib.pyplot as plt
import numpy as np
import mmcv
from shapely.geometry.polygon import Polygon
from mmdet.apis import (inference_detector, init_detector, show_result_pyplot)
import tifffile

from module import utils


logger = logging.getLogger('__main__')


class Detector:
    """A detector object is associated with only one image
    """

    def __init__(self, img, args, csv_writer=None, coco_json=None, confidence_threshold=0.9, rotate=False):
        """Initialize the detector's parameters
        :param img: BGR image of the microplot
        :param func_args: dict: "window_dim": dimensions of the patches
                                "step": step between each patch
                                "config_file": path of the config file of the deep learning model
                                "trained_model": path of the .pth file of the deep learing model
                                "path": path of the result directory
                                "name": name of the image
        :param csv_writer: object allowing to write in the csv result file
        :param coco_json: dict structured like a cocojson object
        """
        self.window_dim = args["window_dim"]
        self.step = args["step"]
        self.config_file = args["config_file"]
        self.trained_model = args["trained_model"]
        self.path = str(args["path"].parent)
        self.name = args["path"].name
        self.output_folder = args["output_folder"]
        self.csv_writer = csv_writer
        self.coco_json = coco_json

        # self.img: raw image
        # self.raw_uplot: raw image.astype(np.uint8)
        # self.uplot: image.astype(np.uint8) with black stripes
        if rotate:
            self.img = utils.rotate_image(img, 0.25, [0, 180])
            # print("printing plot shapes")
            # print(self.img.shape[0])
            # print(self.img.shape[1])

        else:
            self.img = img

        uplot = img[:, :, ::-1]
        # Add black strips around the plot
        self.uplot = np.zeros((uplot.shape[0] + 2 * self.window_dim[0], uplot.shape[1] + 2 * self.window_dim[1], 3))
        self.uplot[self.window_dim[0]: self.window_dim[0] + uplot.shape[0],
        self.window_dim[1]: self.window_dim[1] + uplot.shape[1], :] = uplot
        self.raw_uplot = uplot.astype(np.uint8)
        self.uplot = self.uplot.astype(np.uint8)

        self.result = None
        self.patches = []
        self.confidence_threshold = confidence_threshold

    def patchify(self):
        """Divide the microplot image in patches
        """
        x_stop = False
        x = 0
        while not x_stop:
            y = 0
            y_stop = False
            y_patches = []
            while not y_stop:
                y_patches.append(self.uplot[x: x + self.window_dim[0], y: y + self.window_dim[1], :])
                # cv2.imshow("hop", self.uplot[x: x + self.window_dim[0], y: y + self.window_dim[1], :])
                # cv2.waitKey(0)
                y = y + self.step
                if y + self.window_dim[1] > self.uplot.shape[1]:
                    y_stop = True

            self.patches.append(y_patches)

            x = x + self.step
            if x + self.window_dim[0] > self.uplot.shape[0]:
                x_stop = True

    def detect(self):
        """Detect the sunflowers in each patch
        """
        try:
            model = init_detector(self.config_file, self.trained_model)
        except Exception:
            raise ValueError("MODEL WEIGHTS FILE IS CORRUPTED OR IS NOT THE CORRECT FILE")

        results = []
        for i in self.patches:
            result = []
            for j in i:
                if np.sum(j) == 0:
                    result.append(None)
                else:
                    result.append(inference_detector(model, j))

                # resultat = inference_detector(model, j)
                # show_result_pyplot(model, j, resultat)
            results.append(result)

        self.result = np.array(results, dtype=object)
        # for i, j in enumerate(img):
        #     show_result_pyplot(model, j, result[i])

    def extract_anns(self):
        # PARAMETERS
        threshold = 0.3

        detected_corners = []
        detected_centers = []
        polygons = []
        masks = []
        for x, i in enumerate(self.result):
            for y, j in enumerate(i):
                if j is not None:
                    for k, l in zip(j[0][0], j[1][0]):
                        # kept = True
                        # for mask in masks:
                        #     IoU = np.where(np.logical_and(np.array(k), np.array(mask)))[0].shape[0] / np.where(np.logical_or(np.array(k), np.array(mask)))[0].shape[0]
                        #     if IoU < threshold:
                        #         kept = True

                        if k[4] > self.confidence_threshold and k[0] > 6 and k[2] < 506 and k[1] > 6 and k[3] < 506:
                            detected_corners.append(k[0:5])
                            detected_centers.append(np.array([y * self.step + (k[0] + k[2]) / 2,
                                                              x * self.step + (k[1] + k[3]) / 2]))
                            polys = utils.from_mask_to_polygon(l)
                            replaced_polys = []
                            for m in polys:
                                replaced_poly = []
                                for n in m:
                                    replaced_poly.append((y * self.step + n[0] - self.window_dim[0],
                                                          x * self.step + n[1] - self.window_dim[1]))
                                replaced_polys.append(replaced_poly)

                            polygons.append(replaced_polys)
                            masks.append(l)

        kept_polys = []
        kept_proba = []
        kept_centers = []

        for tested_poly, corner, center in zip(polygons, detected_corners, detected_centers):
            kept = True
            for iterated_poly in kept_polys:
                # x, y = Polygon(iterated_poly[0]).exterior.xy
                # plt.plot(x, y)
                # x, y = Polygon(tested_poly[0]).exterior.xy
                # plt.plot(x, y)
                # plt.show()
                IoU = utils.calculate_IoU(iterated_poly, tested_poly)
                if IoU > threshold:
                    kept = False

            if kept:
                kept_polys.append(tested_poly)
                kept_proba.append(corner[4])
                kept_centers.append(center)

        filtered_polys = []

        for poly in kept_polys:
            new_poly = []
            for subpoly in poly:
                if len(subpoly) > 3:
                    new_poly.append(subpoly)

            filtered_polys.append(new_poly)

        if len(filtered_polys) > 0:
            self.save_data(filtered_polys, kept_proba)
            return filtered_polys, kept_proba, np.array(kept_centers)
        else:
            self.save_data(filtered_polys, kept_proba)
            return False, False, False

    def save_to_coco(self, polygons, proba):
        """Add an image and the annotations corresponding to this image to the coco json variable
        :param polygons: polygons delimiting the sunflowers: [(x, y), ...]
        """
        self.coco_json.add_img_to_coco(self.img, self.path, self.name)
        if len(polygons) > 0:
            self.coco_json.add_anns_to_coco(polygons, proba)

    def save_data(self, filtered_polygons, proba):
        """Write in the csv results file and add annotations and the corresponding image to the coco json variable
        :param filtered_polygons: list of polygons delimiting the sunflowers: [(x, y), ...]
        :param proba : the probability according to the model that the object is indeed what the model is looking for
        """
        if self.csv_writer is not None:
            self.csv_writer.writerow(
                {'plot_name': self.name, 'number_of_sunflowers': str(len(filtered_polygons))})
        if self.coco_json is not None:
            self.save_to_coco(filtered_polygons, proba)

    def run(self):
        """Run the sunflower detection

        :return:
        """
        self.patchify()
        logging.debug("Starting detection...")
        self.detect()
        logging.debug("Done ! \n \n")
        logging.debug("Starting extraction of the objects from the detections...")
        filtered_polygons, proba, detected_centers = self.extract_anns()
        logging.debug("Done ! \n \n")
        if self.raw_uplot.shape[1] < 15000 and self.raw_uplot.shape[0] < 15000:
            plt.figure(figsize=(int(self.raw_uplot.shape[1] / 100), int(self.raw_uplot.shape[0] / 100)), dpi=100)

        if detected_centers is not False:
            prob_xy = detected_centers
            # Plot of the selected annotations (nothing more)
            if self.raw_uplot.shape[1] < 15000 and self.raw_uplot.shape[0] < 15000:
                plt.scatter(detected_centers[:, 0] - self.window_dim[0], detected_centers[:, 1] - self.window_dim[1])
            for i in range(len(proba)):
                for j in filtered_polygons[i]:
                    poly = Polygon(j)
                    x, y = poly.exterior.xy
                    if self.raw_uplot.shape[1] < 15000 and self.raw_uplot.shape[0] < 15000:
                        plt.plot(x, y, color='#6699cc', alpha=0.7, linewidth=3, solid_capstyle='round', zorder=2)

                value = proba[i]
                text = str(int(100 * value) / 100)
                x = prob_xy[i][0] - self.window_dim[0]
                y = prob_xy[i][1] - self.window_dim[1]
                if self.raw_uplot.shape[1] < 15000 and self.raw_uplot.shape[0] < 15000:
                    plt.annotate(text, xy=(x, y))

        Path(str(self.output_folder) + "\\annotated_images").mkdir(parents=True, exist_ok=True)

        # plt.show()
        if self.raw_uplot.shape[1] < 15000 and self.raw_uplot.shape[0] < 15000:
            plt.imshow(self.raw_uplot[:, :, ::-1])
            if detected_centers is not False:
                name = str(self.output_folder) + "\\annotated_images\\" + self.name
            else:
                name = str(self.output_folder) + "\\annotated_images\\no_detection_" + self.name
            plt.savefig(name, )
            plt.cla()
            plt.close()
        return self.coco_json


def run_detection(output_folder, input_folder, config_path, model_path, conf_thresh, step_ratio=1 / 4, rotate=False):
    # create the folder for the counting production chain
    output_folder.mkdir(parents=True, exist_ok=True)
    output_folder = str(output_folder)

    # prepare the parameters of Detector
    config_mmcv = mmcv.Config.fromfile(config_path)
    patch_dims = config_mmcv._cfg_dict.img_scale
    step = int(patch_dims[0] * step_ratio)
    plot_image_name_list = input_folder.glob('*.jpg')
    # plot_image_name_list = itertools.chain(plot_image_name_list, input_folder.glob('*.jpg'))
    # plot_image_name_list = itertools.chain(plot_image_name_list, input_folder.glob('*.PNG'))
    plot_image_name_list = itertools.chain(plot_image_name_list, input_folder.glob('*.png'))
    plot_image_name_list = itertools.chain(plot_image_name_list, input_folder.glob('*.tif'))

    if len(list(itertools.tee(plot_image_name_list))) == 0:
        raise Exception("NO IMAGES IN THE SELECTED IMAGES FOLDER. END OF PROCESS")

    coco = utils.CocoJson(["plants"], year=2020)

    for i in plot_image_name_list:
        logging.debug(f"WORKING ON PLOT: %s" % Path(i).name)
        if str(i)[-4:] == ".tif":
            image = tifffile.imread(str(i))
        else:
            image = cv2.imread(str(i))[:, :, ::-1]

        detector_args = {"window_dim": patch_dims,
                         "step": step,
                         "config_file": str(config_path),
                         "trained_model": str(model_path),
                         "path": i,
                         "output_folder": output_folder}
        detector = Detector(image, detector_args, coco_json=coco, confidence_threshold=conf_thresh, rotate=rotate)
        coco = detector.run()

    if coco is not None:
        now = datetime.now()
        current_time = now.strftime("%y-%m-%d_%H-%M-%S")
        with open(output_folder + "\\" + current_time + '_coco.json', 'w') as f:
            json.dump(coco.coco_file, f, indent=4)
        logging.debug("coco file created")


def run(output_folder, input_folder, config_path, weights_path, confidence_threshold, rotate):
    run_detection(
        Path(output_folder),
        Path(input_folder),
        Path(config_path),
        Path(weights_path),
        confidence_threshold/100,
        rotate=rotate
    )

