import numpy as np
from shapely.geometry import Polygon
import cv2
import imutils
import logging
import json


logger = logging.getLogger('__main__')


def calculate_IoU(polys_1, polys_2):
    intersection = 0
    union = 0
    added = False
    for poly_1 in polys_1:
        if len(poly_1) > 3:
            polygon1 = Polygon(poly_1)
            if not polygon1.is_valid:
                polygon1 = polygon1.buffer(0)

            union += polygon1.area
            for poly_2 in polys_2:
                if len(poly_2) > 3:
                    polygon2 = Polygon(poly_2)
                    if not polygon2.is_valid:
                        polygon2 = polygon2.buffer(0)

                    if not added:
                        union += polygon2.area
                    intersection += polygon1.intersection(polygon2).area

        added = True
    union = polygon1.union(polygon2).area
    if union > 0:
        iou = intersection / union
        return iou
    return 1


def from_mask_to_polygon(mask):
    """Convert a mask into a polygon
    :param mask: ndarray of True and False
    :return result: list of coordinates of the polygon points: [(x, y), ...]
    """
    polygon = np.zeros((mask.shape[0], mask.shape[1]))
    test = np.zeros((mask.shape[0], mask.shape[1]))
    polygon[np.where(mask)] = 255
    ret, thresh = cv2.threshold(polygon, 127, 255, cv2.THRESH_BINARY)
    thresh = np.uint8(thresh)
    contours, hierarchy = cv2.findContours(thresh.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    result = []
    drawing = []
    for contour in contours:
        polys = []
        for i in contour:
            polys.append((i[0, 0], i[0, 1]))
            drawing.append(i[0])

        result.append(polys)

    # cv2.drawContours(polygon, contours, -1, (0, 255, 0), 10)
    drawing = np.array(drawing)
    # plt.scatter(drawing[:, 0], drawing[:, 1])
    # plt.imshow(polygon)
    # plt.show()
    return result


def rotate_image(image, step, angle_range):
    # CALCULATE AND APPLY THE BEST ROTATION
    min = image.shape[1] + 1000
    right_angle = None
    # plt.imshow(self.image)
    # plt.show()

    step_1 = step * 20
    step_2 = step

    for angle in np.arange(angle_range[0], angle_range[1], step_1):
        rotated = imutils.rotate_bound(image, angle)
        hist = np.sum(rotated, axis=(0, 2))
        nb_non_null_column = len(np.where(hist > 0)[0])
        if nb_non_null_column < min:
            min = nb_non_null_column
            right_angle = angle
    rotated_img = imutils.rotate_bound(image, right_angle)

    # plt.imshow(rotated_img)
    # plt.show()

    min += 1000
    for angle in np.arange(-3, 3, step_2):
        rotated = imutils.rotate_bound(rotated_img, angle)
        # plt.imshow(rotated)
        # plt.show()
        hist = np.sum(rotated, axis=(0, 2))
        nb_non_null_column = len(np.where(hist > 0)[0])
        if nb_non_null_column < min:
            min = nb_non_null_column
            right_angle = angle
    rotated_img = imutils.rotate_bound(rotated_img, right_angle)

    return rotated_img


class CocoJson:
    def __init__(self, categories, year=2021):
        """
        :param categories: a list of the names of the categories wanted
        :param year:
        """
        self.coco_file = {

            "info": {
                "year": int(year)
            },
            "categories": [],
            "images": [],
            "annotations": []
        }

        for n, i in enumerate(categories):
            self.coco_file["categories"].append({
                "id": int(n + 1),
                "name": i,
                "supercategory": " "
            })

    def add_img_to_coco(self, img, path, name):
        """
        add info about the image to the coco_json json file
        :param img: image (numpy.ndarray)
        :param path: path of the directory where is the image
        :param name of the image
        """
        ############ Reçoit les bonnes infos ==> id patch et nom de l'image doivent être à jour

        # increment the image id
        if not self.coco_file["images"]:
            img_id = 1
        else:
            img_id = self.coco_file["images"][-1]["id"] + 1

        # fill the coco_json annotation dictionnary
        logging.debug("image number: " + str(img_id))
        coco_images = {
            "id": int(img_id),
            "path": path + "\\" + name,
            "width": int(img.shape[1]),
            "height": int(img.shape[0]),
            "file_name": name,
            "metadata": {}
        }

        self.coco_file["images"].append(coco_images)

    def add_anns_to_coco(self, anns, proba=None):
        """
        add all instance data to the coco_json file for one image
        :param anns: the keypoints to add with this form: [(a, b), (c, d), ...]
        """
        ############ Reçoit les bonnes infos ==> id patch et nom de l'image doivent être à jour
        for i, M in enumerate(anns):
            # increment the annotation id
            if not self.coco_file["annotations"]:
                ann_id = 1
            else:
                ann_id = self.coco_file["annotations"][-1]["id"] + 1

            flat_x = []
            flat_y = []

            total_anns = []
            area = 0
            for N in M:
                if len(N) > 3:
                    # flaten the annotations from [(a, b), (c, d)] to [a, b, c, d]
                    flat_anns = []
                    for P in N:
                        flat_anns.append(int(P[0]))
                        flat_x.append(int(P[0]))
                        flat_anns.append(int(P[1]))
                        flat_y.append(int(P[1]))

                    # calculate the area
                    poly = Polygon(tuple(N))
                    area += float(poly.area)

                    total_anns.append(flat_anns)

            area = float(area)

            if flat_y and flat_x:
                # calculate the bbox dimensions
                flat_x = np.array(flat_x)
                flat_y = np.array(flat_y)

                bbox_dims = [int(np.min(flat_x)), int(np.min(flat_y)),
                             int(np.max(flat_x) - np.min(flat_x)), int(np.max(flat_y) - np.min(flat_y))]

                centroid = [bbox_dims[0] + bbox_dims[2] / 2, bbox_dims[1] + bbox_dims[3] / 2]

                # fill the coco_json annotation dictionnary
                if proba is None:
                    coco_anns = {
                        "id": int(ann_id),
                        "image_id": int(self.coco_file["images"][-1]["id"]),
                        "category_id": int(1),
                        "segmentation": total_anns,
                        "bbox": bbox_dims,
                        "centroid": centroid,
                        "area": area,
                        "iscrowd": int(0)
                    }
                if proba is not None:
                    coco_anns = {
                        "id": int(ann_id),
                        "image_id": int(self.coco_file["images"][-1]["id"]),
                        "category_id": int(1),
                        "segmentation": total_anns,
                        "bbox": bbox_dims,
                        "centroid": centroid,
                        "certainty": float(proba[i]),
                        "area": area,
                        "iscrowd": int(0)
                    }

                self.coco_file["annotations"].append(coco_anns)
