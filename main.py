"""
Command Line Interface (CLI) manager for My Module.

This module takes as argument the path to a folder containing images with objects to detect, and the paths to the config
file and the weights of the MaskRCNN model used. It then detects the objects on the images and stores it into a json
file in coco format
"""
import argparse
import logging
import os
import time
import sys

from module import uav_rgb_objectdetection_all
from module import error

PROCESS_NAME = "UAV RGB Object Detection All"


def create_parser():
    """Create Command Line Interface arguments parser
     """
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "output_folder",
        help="path to the directory where the output will be generated")

    parser.add_argument(
        "input_folder",
        help="path to the directory where the images are stored")

    parser.add_argument(
        "config_path",
        help="path to the MaskRCNN model config file")

    parser.add_argument(
        "model_path",
        help="path to the MaskRCNN model weights")

    parser.add_argument(
        '-c', '--confidence_threshold',
        type=int,
        default=80,
        help='Integer that represents the confidence threshold value in percents that determines if a detected object is a sunflower or not. Default: 80'
    )

    parser.add_argument(
        '-r', '--rotate',
        action='store_true',
        help='indicate if plot rotation to save computational power is needed.'
    )

    parser.add_argument(
        "-v",
        "--verbose",
        help="activate output verbosity",
        action="store_true",
        default=False)

    return parser.parse_args()


if __name__ == '__main__':
    timer_start = time.time()
    try:
        # Manage CLI arguments
        args = create_parser()

        # Init Logger
        logger = logging.getLogger()

        if args.verbose:
            logger.setLevel(logging.DEBUG)
        else:
            logger.setLevel(logging.INFO)

        logger.addHandler(logging.StreamHandler(sys.stdout))
        logger.addHandler(logging.FileHandler(os.path.splitext(__file__)[0] + '.log', 'w'))

        logging.debug("CLI arguments are valid")

        # Creating output folder if doesn't exist
        if not os.path.exists(args.output_folder):
            logging.debug("Create output folder '" + args.output_folder + "'")
            os.mkdir(args.output_folder)

        # Run module core process
        uav_rgb_objectdetection_all.run(
            output_folder=args.output_folder,
            input_folder=args.input_folder,
            config_path=args.config_path,
            weights_path=args.model_path,
            confidence_threshold=args.confidence_threshold,
            rotate=args.rotate
        )

        logging.info("Process '" + PROCESS_NAME + "' complete")

    except error.DataError as e:
        # When DataError is raised, the process fails without killing the overall execution
        logging.exception(e)
        logging.error("Process '" + PROCESS_NAME + "' failed")
    except Exception as e:
        # Every other errors kill the overall execution
        logging.exception(e)
        logging.error("Process '" + PROCESS_NAME + "' failed")
        sys.exit(1)
    finally:
        timer_end = time.time()
        logging.debug("Process '" + PROCESS_NAME + "' took " + str(int(timer_end - timer_start)) + " seconds")
